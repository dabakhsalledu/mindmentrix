
from flask import Flask, request, jsonify
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.impute import SimpleImputer
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

def load_data_housing():
    colnames = ['school', 'sex', 'age', 'address', 'famsize', 'Pstatus', 'Medu', 'Fedu', 'Mjob', 'Fjob', 'reason', 'guardian', 'traveltime', 'studytime', 'failures', 'schoolsup', 'famsup', 'paid', 'activities', 'nursery', 'higher', 'internet', 'romantic', 'famrel', 'freetime', 'goout', 'Dalc', 'Walc', 'health', 'absences', 'G1', 'G2', 'G3']
    data = pd.read_csv('../student_data.csv', names=colnames, delimiter=',', header=0)
    
    # Réorganiser les colonnes dans le bon ordre
    data = data[colnames]

    return data


housing_data = load_data_housing()

def predict_grade(user_input):
    try:
        global housing_data

        print('amadou1')

        # Convertir les colonnes numériques en types numériques
        numeric_columns = ['age', 'Medu', 'Fedu', 'traveltime', 'studytime', 'failures', 'famrel', 'freetime', 'goout', 'Dalc', 'Walc', 'health', 'absences', 'G1', 'G2', 'G3']
        housing_data[numeric_columns] = housing_data[numeric_columns].apply(pd.to_numeric)
        print('Inside predict_grade')

        # Diviser les données en variables explicatives (X) et cible (Y)
        X = housing_data.drop(columns='G3')
        Y = housing_data['G3']

        print('Before one-hot encoding')
        # Réaliser l'encodage one-hot pour les colonnes catégorielles seulement
        categorical_cols = housing_data.select_dtypes(include=['object']).columns
        print("Colonnes catégorielles avant l'encodage one-hot:", categorical_cols)

        for col in categorical_cols:
            if col != 'G3':  # Éviter d'encoder la cible 'G3'
                housing_data = pd.get_dummies(housing_data, columns=[col])

        categorical_cols_after = housing_data.select_dtypes(include=['object']).columns
        print("Colonnes catégorielles après l'encodage one-hot:", categorical_cols_after)
        print('After one-hot encoding')

        # Après la conversion des colonnes numériques
        numeric_cols_after_conversion = housing_data.select_dtypes(include=['number']).columns
        print("Colonnes numériques après la conversion:", numeric_cols_after_conversion)


        # Créer et entraîner le modèle de régression linéaire
        model = LinearRegression()
        model.fit(X, Y)

        print('After model fitting')

                # Créer le DataFrame pour les données utilisateur
        user_input_df = pd.DataFrame([user_input], columns=housing_data.drop(columns='G3').columns)

        # Assurez-vous que les colonnes des données utilisateur correspondent à celles du modèle pour l'encodage one-hot
        categorical_cols = housing_data.select_dtypes(include=['object']).columns
        user_input_categorical = user_input_df[categorical_cols]

        # Réaliser l'encodage one-hot pour les données utilisateur
        user_input_categorical_encoded = pd.get_dummies(user_input_categorical)

        # Combinez les colonnes encodées avec les colonnes numériques
        user_input_numerical = user_input_df.drop(columns=categorical_cols)
        user_input_df_encoded = pd.concat([user_input_numerical, user_input_categorical_encoded], axis=1)

        print("Before prediction")
        # Vérifiez que toutes les colonnes nécessaires pour la prédiction sont présentes dans user_input_df_encoded

        # Assurez-vous que les colonnes de user_input_df_encoded correspondent à celles du modèle
        expected_columns = set(housing_data.columns) - {'G3'}  # Colonnes utilisées pour l'entraînement du modèle
        missing_cols = set(expected_columns) - set(user_input_df_encoded.columns)

        for col in missing_cols:
            user_input_df_encoded[col] = 0

        user_input_df_encoded = user_input_df_encoded[expected_columns]  # Ordonnez les colonnes comme dans le modèle

        # Faire la prédiction
        user_prediction = model.predict(user_input_df_encoded)
        print("After prediction")
        print("Prédiction:", user_prediction)

        # Vérifier si la prédiction est 5
        if user_prediction[0] == 5:
            return "A"  # Retourner la note A si la prédiction est 5

        return user_prediction[0] if user_prediction is not None else "No predict"  # Autrement, retourner la prédiction  # Assurez-vous de retourner une valeur par défaut si la prédiction est nulle


        # # Créer le DataFrame pour les données utilisateur
        # user_input_df = pd.DataFrame([user_input], columns=housing_data.drop(columns='G3').columns)
        # # Réaliser l'encodage one-hot pour les données utilisateur
        # user_input_df = pd.get_dummies(user_input_df, columns=user_input_df.columns)

        # print("Before prediction")
        # # Faire la prédiction
        # user_prediction = model.predict(user_input_df)
        # print("After prediction")

        # print("Prédiction:", user_prediction)

        # #return user_prediction[0]
        # return user_prediction if user_prediction is not None else "77"

    except Exception as e:
        print(f"An error occurred: {str(e)}")
        return None


@app.route('/load_housing_data', methods=['GET'])
def get_housing_data():
    print("Before to_dict")  # Ajouter ce print
    housing_data_dict = housing_data.to_dict(orient='list')
    print("After to_dict")  # Ajouter ce print
    #print(housing_data_dict)
    return jsonify(housing_data_dict)

@app.route('/predict', methods=['POST'])
def predict():
    user_input = request.json
    try:
        X = housing_data.drop(columns='G3')
        Y = housing_data['G3']
        print('amadou3')

        user_input_filtered = {key: value for key, value in user_input.items() if value}
        
        print("Before predict_grade")  # Ajouter ce print


                # Vérifier les données de la colonne 'age'
        unique_age_values = housing_data['age'].unique()
        unique_school_values = housing_data['school'].unique()
        print("Valeurs uniques dans la colonne 'age':", unique_age_values)
        print("Valeurs uniques dans la colonne 'school':", unique_school_values)

        # Identifier et nettoyer les valeurs non numériques
        try:
            housing_data['age'] = pd.to_numeric(housing_data['age'], errors='coerce')
            print("Conversion des données de la colonne 'age' en données numériques réussie.")
            # Vérifier les valeurs uniques dans la colonne 'school'
            unique_school_values = housing_data['school'].unique()
            print("Valeurs uniques dans la colonne 'school':", unique_school_values)

        except ValueError as ve:
            print(f"Erreur lors de la conversion des données de la colonne 'age': {str(ve)}")



        user_prediction = predict_grade(user_input_filtered)
        print("After predict_grade")  # Ajouter ce print
        
        print("Prédiction:", user_prediction)
        print('amadou4')  # Ajout du print ici
        
        # Assurez-vous que les colonnes des données utilisateur correspondent à celles du modèle
        user_input_df = pd.DataFrame([user_input_filtered], columns=housing_data.drop(columns='G3').columns)
        
        # Réalisez l'encodage one-hot pour les données utilisateur
        user_input_df = pd.get_dummies(user_input_df, columns=user_input_df.columns)

        # Assurez-vous que les colonnes correspondent
        missing_cols = set(housing_data.columns) - set(user_input_df.columns)
        for col in missing_cols:
            user_input_df[col] = 0

        # Assurez-vous que les colonnes sont dans le même ordre que celles du modèle
        user_input_df = user_input_df[housing_data.columns]

        return jsonify({"prediction": "5"})
    except Exception as e:
        return jsonify({"error": str(e)})

if __name__ == '__main__':
    app.run(debug=True)
