from flask import Flask, jsonify, request
from controllers import create,login
from app import *
# import openai
# from flask_cors import CORS  # Importez CORS depuis flask_cors
# app = Flask(__name__)
# CORS(app)  # Activez CORS pour votre application Flask
 
# openai.api_key = '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'
 
# def generate_response(user_input):
#     completions = openai.Completion.create(
#         engine='text-davinci-003',
#         prompt=user_input,
#         max_tokens=1024,
#         n=1,
#         stop=None,
#         temperature=0.5,
#     )
#     message = completions.choices[0].text
#     return message
 
# @app.route('/api/chatbot', methods=['POST'])
# def chatbot():
#     data = request.get_json()
#     user_input = data['user_input']
#     print(user_input)
#     output = generate_response(user_input)
#     return jsonify({'response': output})

from PyPDF2 import PdfReader
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.text_splitter import CharacterTextSplitter
from langchain.vectorstores import elastic_vector_search,pinecone,weaviate,FAISS
from langchain.chains.question_answering import load_qa_chain
from langchain.chat_models import ChatOpenAI
import os
from flask_cors import CORS  # Importez CORS depuis flask_cors
app = Flask(__name__)
CORS(app)  # Activez CORS pour votre application Flask
 
 
os.environ['OPENAI_API_KEY'] = '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'
chemin_pdf = r"C:\Users\dabak\OneDrive\Documents\sesseion6\mendmentrix\frontend\src\chatpdf1.pdf"
def extract_pdf():
    with open(chemin_pdf, "rb") as fichier_pdf:
        reader=PdfReader(fichier_pdf)
        raw_text=''
        for i,page in enumerate(reader.pages):
            text=page.extract_text()
            if text:
                raw_text+=text
 
    splitter = CharacterTextSplitter(
        separator="\n",
        chunk_size=1000,
        chunk_overlap=200,
        length_function=len
    )
    chunks = splitter.split_text(raw_text)
    return chunks
 
def create_vectorstore():
    chunks=extract_pdf()
    current_embedding = OpenAIEmbeddings()
    vectorestore = FAISS.from_texts(texts=chunks, embedding=current_embedding)
    return vectorestore
 
def generate(input_user):
    chain=load_qa_chain(ChatOpenAI(),chain_type="stuff")
    docsearch=create_vectorstore()
    docs=docsearch.similarity_search(input_user)
    response=chain.run(input_documents=docs,question=input_user)
    return response
 
@app.route('/api/chatbot', methods=['POST'])
def chatbot():
    data = request.get_json()
    user_input = data['user_input']
    output = generate(user_input)
    return jsonify({'response': output})


 
@app.route('/api/register', methods=['POST'])
def register():
    data = request.get_json()
    res=create(data)
    if res:
        response = {'success':'False'}
    else:
        response = {'success':'True'}
    return jsonify(response)
 
@app.route('/api/login', methods=['POST'])
def Login():
    data = request.get_json()
    user,res=login(data)
    if res:
        response = {'success':'True','data':user}
    else:
        response = {'success':'False','data':None}
    return jsonify(response)



# from flask import Flask, request, jsonify
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.impute import SimpleImputer
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder
#from flask_cors import CORS

#app = Flask(__name__)
#CORS(app)

def load_data_housing():
    colnames = ['school', 'sex', 'age', 'address', 'famsize', 'Pstatus', 'Medu', 'Fedu', 'Mjob', 'Fjob', 'reason', 'guardian', 'traveltime', 'studytime', 'failures', 'schoolsup', 'famsup', 'paid', 'activities', 'nursery', 'higher', 'internet', 'romantic', 'famrel', 'freetime', 'goout', 'Dalc', 'Walc', 'health', 'absences', 'G1', 'G2', 'G3']
    data = pd.read_csv('../student_data.csv', names=colnames, delimiter=',', header=0)
    
    # Réorganiser les colonnes dans le bon ordre
    data = data[colnames]

    return data


housing_data = load_data_housing()

def predict_grade(user_input):
    try:
        global housing_data

        print('amadou1')

        # Convertir les colonnes numériques en types numériques
        numeric_columns = ['age', 'Medu', 'Fedu', 'traveltime', 'studytime', 'failures', 'famrel', 'freetime', 'goout', 'Dalc', 'Walc', 'health', 'absences', 'G1', 'G2', 'G3']
        housing_data[numeric_columns] = housing_data[numeric_columns].apply(pd.to_numeric)
        print('Inside predict_grade')

        # Diviser les données en variables explicatives (X) et cible (Y)
        X = housing_data.drop(columns='G3')
        Y = housing_data['G3']

        print('Before one-hot encoding')
        # Réaliser l'encodage one-hot pour les colonnes catégorielles seulement
        categorical_cols = housing_data.select_dtypes(include=['object']).columns
        print("Colonnes catégorielles avant l'encodage one-hot:", categorical_cols)

        for col in categorical_cols:
            if col != 'G3':  # Éviter d'encoder la cible 'G3'
                housing_data = pd.get_dummies(housing_data, columns=[col])

        categorical_cols_after = housing_data.select_dtypes(include=['object']).columns
        print("Colonnes catégorielles après l'encodage one-hot:", categorical_cols_after)
        print('After one-hot encoding')

        # Après la conversion des colonnes numériques
        numeric_cols_after_conversion = housing_data.select_dtypes(include=['number']).columns
        print("Colonnes numériques après la conversion:", numeric_cols_after_conversion)


        # Créer et entraîner le modèle de régression linéaire
        model = LinearRegression()
        model.fit(X, Y)

        print('After model fitting')

                # Créer le DataFrame pour les données utilisateur
        user_input_df = pd.DataFrame([user_input], columns=housing_data.drop(columns='G3').columns)

        # Assurez-vous que les colonnes des données utilisateur correspondent à celles du modèle pour l'encodage one-hot
        categorical_cols = housing_data.select_dtypes(include=['object']).columns
        user_input_categorical = user_input_df[categorical_cols]

        # Réaliser l'encodage one-hot pour les données utilisateur
        user_input_categorical_encoded = pd.get_dummies(user_input_categorical)

        # Combinez les colonnes encodées avec les colonnes numériques
        user_input_numerical = user_input_df.drop(columns=categorical_cols)
        user_input_df_encoded = pd.concat([user_input_numerical, user_input_categorical_encoded], axis=1)

        print("Before prediction")
        # Vérifiez que toutes les colonnes nécessaires pour la prédiction sont présentes dans user_input_df_encoded

        # Assurez-vous que les colonnes de user_input_df_encoded correspondent à celles du modèle
        expected_columns = set(housing_data.columns) - {'G3'}  # Colonnes utilisées pour l'entraînement du modèle
        missing_cols = set(expected_columns) - set(user_input_df_encoded.columns)

        for col in missing_cols:
            user_input_df_encoded[col] = 0

        user_input_df_encoded = user_input_df_encoded[expected_columns]  # Ordonnez les colonnes comme dans le modèle

        # Faire la prédiction
        user_prediction = model.predict(user_input_df_encoded)
        print("After prediction")
        print("Prédiction:", user_prediction)

        # Vérifier si la prédiction est 5
        if user_prediction[0] == 5:
            return "A"  # Retourner la note A si la prédiction est 5

        return user_prediction[0] if user_prediction is not None else "No predict"  # Autrement, retourner la prédiction  # Assurez-vous de retourner une valeur par défaut si la prédiction est nulle

    except Exception as e:
        print(f"An error occurred: {str(e)}")
        return None


@app.route('/load_housing_data', methods=['GET'])
def get_housing_data():
    print("Before to_dict")  # Ajouter ce print
    housing_data_dict = housing_data.to_dict(orient='list')
    print("After to_dict")  # Ajouter ce print
    #print(housing_data_dict)
    return jsonify(housing_data_dict)

@app.route('/predict', methods=['POST'])
def predict():
    user_input = request.json
    try:
        X = housing_data.drop(columns='G3')
        Y = housing_data['G3']
        print('amadou3')

        user_input_filtered = {key: value for key, value in user_input.items() if value}
        
        print("Before predict_grade")  # Ajouter ce print


                # Vérifier les données de la colonne 'age'
        unique_age_values = housing_data['age'].unique()
        unique_school_values = housing_data['school'].unique()
        print("Valeurs uniques dans la colonne 'age':", unique_age_values)
        print("Valeurs uniques dans la colonne 'school':", unique_school_values)

        # Identifier et nettoyer les valeurs non numériques
        try:
            housing_data['age'] = pd.to_numeric(housing_data['age'], errors='coerce')
            print("Conversion des données de la colonne 'age' en données numériques réussie.")
            # Vérifier les valeurs uniques dans la colonne 'school'
            unique_school_values = housing_data['school'].unique()
            print("Valeurs uniques dans la colonne 'school':", unique_school_values)

        except ValueError as ve:
            print(f"Erreur lors de la conversion des données de la colonne 'age': {str(ve)}")



        user_prediction = predict_grade(user_input_filtered)
        print("After predict_grade")  # Ajouter ce print
        
        print("Prédiction:", user_prediction)
        print('amadou4')  # Ajout du print ici
        
        # Assurez-vous que les colonnes des données utilisateur correspondent à celles du modèle
        user_input_df = pd.DataFrame([user_input_filtered], columns=housing_data.drop(columns='G3').columns)
        
        # Réalisez l'encodage one-hot pour les données utilisateur
        user_input_df = pd.get_dummies(user_input_df, columns=user_input_df.columns)

        # Assurez-vous que les colonnes correspondent
        missing_cols = set(housing_data.columns) - set(user_input_df.columns)
        user_input_df = pd.concat([user_input_df, pd.DataFrame({col: [0] * len(user_input_df) for col in missing_cols})], axis=1)


        # Assurez-vous que les colonnes sont dans le même ordre que celles du modèle
        user_input_df = user_input_df[housing_data.columns]

        return jsonify({"prediction": "5"})
    except Exception as e:
        return jsonify({"error": str(e)})

# if __name__ == '__main__':
#     app.run(debug=True)


# Route pour recevoir les résultats du quiz
@app.route('/api/quiz/results', methods=['POST'])
def receive_quiz_results():
    quiz_results = request.json.get('quizResults')

    # Analyser les résultats du quiz et générer des recommandations en fonction du score, des réponses, etc.
    recommendations = generate_recommendations(quiz_results)
    print(recommendations)
    # Renvoyer les recommandations au frontend
    return jsonify(recommendations)

# Fonction pour générer des recommandations en fonction des résultats du quiz
def generate_recommendations(quiz_results):
    recommendations = []

    # Analysez les résultats du quiz pour déterminer les domaines d'intérêt de l'utilisateur
    # C'est ici que vous pourriez utiliser les réponses du quiz pour identifier les intérêts de l'utilisateur

    # Exemple simple : si l'utilisateur a bien répondu à une majorité de questions sur l'intelligence artificielle
    # Ajoutez des recommandations liées à l'intelligence artificielle
    if quiz_results.get("ia_score", 0) > 5:
        recommendations.append({
            "title": "Cours d'intelligence artificielle",
            "link": "https://example.com/courses/ai",
            "description": "Découvrez des cours sur l'intelligence artificielle"
        })

    # Exemple : si l'utilisateur a bien répondu à des questions sur Python
    # Ajoutez des recommandations de cours sur Python
    if quiz_results.get("python_score", 0) > 3:
        recommendations.append({
            "title": "Cours de Python",
            "link": "https://example.com/courses/python",
            "description": "Explorez des cours sur Python pour améliorer vos compétences"
        })

    # Ajoutez d'autres recommandations en fonction des résultats du quiz

    return recommendations

# Utilisation de la fonction generate_recommendations avec les résultats du quiz (quiz_results)
# Remplacez le dictionnaire quiz_results par les résultats réels du quiz
quiz_results = {
    "ia_score": 7,
    "python_score": 4,
    # Ajoutez d'autres scores pour d'autres domaines ou sujets
}

recommendations = generate_recommendations(quiz_results)
print(recommendations)  # Affichez les recommandations générée


if __name__ == "__main__":
    app.run(debug=True)