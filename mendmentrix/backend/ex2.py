# from flask import Flask, request, jsonify
# import pandas as pd
# from sklearn.linear_model import LinearRegression
# from flask_cors import CORS

# app = Flask(__name__)
# CORS(app)

# def load_data_housing():
#     colnames = ['school', 'sex', 'age', 'address', 'famsize', 'Pstatus', 'Medu', 'Fedu', 'Mjob', 'Fjob', 'reason', 'guardian', 'traveltime', 'studytime', 'failures', 'schoolsup', 'famsup', 'paid', 'activities', 'nursery', 'higher', 'internet', 'romantic', 'famrel', 'freetime', 'goout', 'Dalc', 'Walc', 'health', 'absences', 'G1', 'G2', 'G3']
#     data = pd.read_csv('../student_data.csv', names=colnames, delimiter=',', header=0)
    
#     # Réorganiser les colonnes dans le bon ordre
#     data = data[colnames]

#     return data

# housing_data = load_data_housing()

# def predict_grade(user_input):
#     try:
#         global housing_data

#         # Convertir les colonnes numériques en types numériques
#         numeric_columns = ['age', 'Medu', 'Fedu', 'traveltime', 'studytime', 'failures', 'famrel', 'freetime', 'goout', 'Dalc', 'Walc', 'health', 'absences', 'G1', 'G2', 'G3']
#         housing_data[numeric_columns] = housing_data[numeric_columns].apply(pd.to_numeric)

#         # Réaliser l'encodage one-hot pour les colonnes catégorielles seulement
#         categorical_cols = housing_data.select_dtypes(include=['object']).columns
#         for col in categorical_cols:
#             if col != 'G3':  # Éviter d'encoder la cible 'G3'
#                 housing_data = pd.get_dummies(housing_data, columns=[col])

#         # Diviser les données en variables explicatives (X) et cible (Y)
#         X = housing_data.drop(columns='G3')
#         Y = housing_data['G3']

#         # Créer et entraîner le modèle de régression linéaire
#         model = LinearRegression()
#         model.fit(X, Y)

#         # Créer le DataFrame pour les données utilisateur
#         user_input_df = pd.DataFrame([user_input], columns=X.columns)
#         # Réaliser l'encodage one-hot pour les données utilisateur
#         user_input_df = pd.get_dummies(user_input_df, columns=user_input_df.columns)

#         # Assurez-vous que les colonnes des données utilisateur correspondent à celles du modèle
#         missing_cols = set(X.columns) - set(user_input_df.columns)
#         for col in missing_cols:
#             user_input_df[col] = 0

#         # Assurez-vous que les colonnes sont dans le même ordre que celles du modèle
#         user_input_df = user_input_df[X.columns]

#         # Faire la prédiction
#         user_prediction = model.predict(user_input_df)

#         return user_prediction[0] if user_prediction is not None else "77"

#     except Exception as e:
#         print(f"An error occurred: {str(e)}")
#         return None

# @app.route('/predict', methods=['POST'])
# def predict():
#     user_input = request.json
#     try:
#         user_prediction = predict_grade(user_input)
#         return jsonify({"prediction": user_prediction})
#     except Exception as e:
#         return jsonify({"error": str(e)})

# if __name__ == '__main__':
#     app.run(debug=True)

from flask import Flask, request, jsonify
import pandas as pd
from sklearn.linear_model import LinearRegression
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

# def load_data_housing():
#     colnames = ['school', 'sex', 'age', 'address', 'famsize', 'Pstatus', 'Medu', 'Fedu', 'Mjob', 'Fjob', 'reason', 'guardian', 'traveltime', 'studytime', 'failures', 'schoolsup', 'famsup', 'paid', 'activities', 'nursery', 'higher', 'internet', 'romantic', 'famrel', 'freetime', 'goout', 'Dalc', 'Walc', 'health', 'absences', 'G1', 'G2', 'G3']
#     data = pd.read_csv('../student_data.csv', names=colnames, delimiter=',', header=0)
    
#     # Réorganiser les colonnes dans le bon ordre
#     data = data[colnames]

#     return data

# housing_data = load_data_housing()
def load_data_housing():
    colnames = ['school', 'sex', 'age', 'address', 'famsize', 'Pstatus', 'Medu', 'Fedu', 'Mjob', 'Fjob', 'reason', 'guardian', 'traveltime', 'studytime', 'failures', 'schoolsup', 'famsup', 'paid', 'activities', 'nursery', 'higher', 'internet', 'romantic', 'famrel', 'freetime', 'goout', 'Dalc', 'Walc', 'health', 'absences', 'G1', 'G2', 'G3']
    data = pd.read_csv('../student_data.csv', names=colnames, delimiter=',', header=0)
    
    # Réorganiser les colonnes dans le bon ordre
    data = data[colnames]

    return data


housing_data = load_data_housing()

def predict_grade(user_input):
    try:
        global housing_data

        # Convertir les colonnes numériques en types numériques
        numeric_columns = ['age', 'Medu', 'Fedu', 'traveltime', 'studytime', 'failures', 'famrel', 'freetime', 'goout', 'Dalc', 'Walc', 'health', 'absences', 'G1', 'G2', 'G3']
        housing_data[numeric_columns] = housing_data[numeric_columns].apply(pd.to_numeric)

        # Réaliser l'encodage one-hot pour les colonnes catégorielles seulement
        categorical_cols = housing_data.select_dtypes(include=['object']).columns
        for col in categorical_cols:
            if col != 'G3':  # Éviter d'encoder la cible 'G3'
                housing_data = pd.get_dummies(housing_data, columns=[col])

        # Diviser les données en variables explicatives (X) et cible (Y)
        X = housing_data.drop(columns='G3')
        Y = housing_data['G3']

        # Créer et entraîner le modèle de régression linéaire
        model = LinearRegression()
        model.fit(X, Y)

        # Créer le DataFrame pour les données utilisateur
        user_input_df = pd.DataFrame([user_input], columns=X.columns)
        # Réaliser l'encodage one-hot pour les données utilisateur
        user_input_df = pd.get_dummies(user_input_df, columns=user_input_df.columns)

        # Assurez-vous que les colonnes des données utilisateur correspondent à celles du modèle
        missing_cols = set(X.columns) - set(user_input_df.columns)
        for col in missing_cols:
            user_input_df[col] = 0

        # Assurez-vous que les colonnes sont dans le même ordre que celles du modèle
        user_input_df = user_input_df[X.columns]

        # Faire la prédiction
        user_prediction = model.predict(user_input_df)

        return user_prediction[0] if user_prediction is not None else "77"

    except Exception as e:
        print(f"An error occurred: {str(e)}")
        return None

@app.route('/load_housing_data', methods=['GET'])
def get_housing_data():
    print("Before to_dict")  # Ajouter ce print
    housing_data_dict = housing_data.to_dict(orient='list')
    print("After to_dict")  # Ajouter ce print
    #print(housing_data_dict)
    return jsonify(housing_data_dict)


@app.route('/predict', methods=['POST'])
def predict():
    user_input = request.json
    try:
        user_prediction = predict_grade(user_input)
        print(user_prediction)
        return jsonify({"prediction": user_prediction})
    except Exception as e:
        return jsonify({"error": str(e)})

if __name__ == '__main__':
    app.run(debug=True)

#ne fais pas partie du projet...test pour resoudre un bug